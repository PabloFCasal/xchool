Rails.application.routes.draw do
  resources :users
  
  root "courses#index"
  
  resources :courses do
    resources :enrollments
  end
end
