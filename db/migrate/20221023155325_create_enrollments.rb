class CreateEnrollments < ActiveRecord::Migration[7.0]
  def change
    create_table :enrollments do |t|
      t.integer :pass_score
      t.float :progress
      t.integer :score
      t.boolean :completed
      t.boolean :graduated
      t.references :course, null: false, foreign_key: true

      t.timestamps
    end
  end
end
