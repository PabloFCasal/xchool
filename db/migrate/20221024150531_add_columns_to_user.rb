class AddColumnsToUser < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :average_progress, :float
    add_column :users, :average_score, :float
    add_column :users, :graduated, :boolean
    add_column :users, :admin, :boolean
  end
end
