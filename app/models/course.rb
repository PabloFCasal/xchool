# == Schema Information
#
# Table name: courses
#
#  id         :integer          not null, primary key
#  title      :string
#  code       :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Course < ApplicationRecord

  has_many :enrollments, dependent: :destroy

  validates :title,  presence: true
  validates :code, numericality: { greater_than: 0 }, length: { maximum: 3 }

end
