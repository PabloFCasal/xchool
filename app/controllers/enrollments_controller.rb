class EnrollmentsController < ApplicationController

  def index
    @course = Course.find(params[:course_id])
    @enrollments = @course.enrollments
  end
end
